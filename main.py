from selenium import webdriver
from selenium.webdriver.common.by import By
import requests
import time
import json



def main():
    # init webdriver
    driver = webdriver.Chrome('chromedriver.exe')  # Change to match your chromedriver
    print("chromedriver initialized")    
    # execute some stuff using webdriver
    driver.get('http://ttl-be-testme.herokuapp.com')
    print("navigated to homepage")
    time.sleep(5) # Let the user actually see something!
    input_box = driver.find_element(By.CSS_SELECTOR, 'input[name="title"]')
    input_box.send_keys('ChromeDriver')
    submit_button = driver.find_element(By.CSS_SELECTOR, 'input[type="submit"]')
    submit_button.click()
    print("input submitted")
    time.sleep(5) # Let the user actually see something!
    # verify the same stuff using requests
    url = "https://ttl-be-testme.herokuapp.com/api/todos"
    payload={}
    headers = {}
    resp = json.loads(requests.request("GET", url, headers=headers, data=payload).text)
    textFound = False
    for todo in resp['todos']:
        if 'ChromeDriver' in todo['todo']:
            textFound = True
    if textFound:
        print("The todo was found in the api")
    else:
        print("The todo was not found in the api")
    # report
    #close & cleanup
    finishedBox = driver.find_element(By.XPATH, "//*[contains(text(),'ChromeDriver')]/input")
    finishedBox.click()
    time.sleep(5) # Let the user actually see something!
    print("checkbox checked")
    resp = json.loads(requests.request("GET", url, headers=headers, data=payload).text)
    todoFinished = False
    for todo in resp['todos']:
        if 'ChromeDriver' in todo['todo']:
            if todo['status'] == 1:
                todoFinished = True
    if todoFinished:
        print("The todo was finished in the api")
    else:
        print("The todo was not finished in the api")

    deleteButton = driver.find_element(By.XPATH, "//*[contains(text(),'ChromeDriver')]/button")
    deleteButton.click()
    time.sleep(5) # Let the user actually see something!
    print("delete clicked")
    resp = json.loads(requests.request("GET", url, headers=headers, data=payload).text)
    textFound = False
    for todo in resp['todos']:
        if 'ChromeDriver' in todo['todo']:
            textFound = True
    if textFound:
        print("The todo was found in the api")
    else:
        print("The todo was not found in the api")






    driver.quit()

main()